package com.example.kropp.mini_projekt1.Activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kropp.mini_projekt1.Database.ProductDBHelper;
import com.example.kropp.mini_projekt1.R;

public class AddProduct extends AppCompatActivity {

    private ProductDBHelper db;
    private Button addProductButton, showListButton;
    private EditText name;
    private TextView infoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        db = new ProductDBHelper(this);
        name = (EditText) findViewById(R.id.productName);
        infoText = (TextView) findViewById(R.id.infoTextView);

        showListButton = (Button) findViewById(R.id.showListButton);
        showProducts();
        addProductButton = (Button) findViewById(R.id.addProductButton);
        addProduct();
    }

    private View.OnClickListener showAllProducts(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cursor res = db.showAllProducts();
                if (res.getCount() == 0) {
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context,"Brak produktów do wyświetlenia", Toast.LENGTH_SHORT);
                    toast.show();
                }

                if (res.getCount() != 0) {
                    Intent intent = new Intent(AddProduct.this, ShowProducts.class);
                    startActivity(intent);
                }
            }
        };
    }

    public void showProducts(){
        showListButton.setOnClickListener(showAllProducts());
    }

    private View.OnClickListener addProductOnClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (name.getText().toString().matches("")){
                    infoText.setTextColor(Color.parseColor("#ff0033"));
                    infoText.setText("Podaj nazwę produktu!");
                    name.setText("");

                    Handler h = new Handler();
                    h.postDelayed(new Runnable(){
                        public void run(){
                            infoText.setText("");
                        }
                    }, 1500);
                } else {
                    db.insertProduct(name.getText().toString());
                    infoText.setTextColor(Color.parseColor("#33cc00"));
                    infoText.setText("Pomyślnie dodano nowy produkt!");
                    name.setText("");

                    Handler h = new Handler();
                    h.postDelayed(new Runnable(){
                        public void run(){
                            infoText.setText("");
                        }
                    }, 1500);
                }
            }
        };
    }

    public void addProduct(){
        addProductButton.setOnClickListener(addProductOnClickListener());
    }
}
