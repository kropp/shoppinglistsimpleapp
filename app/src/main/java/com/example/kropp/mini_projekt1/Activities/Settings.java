package com.example.kropp.mini_projekt1.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kropp.mini_projekt1.R;

import static android.R.attr.defaultValue;

public class Settings extends AppCompatActivity {

    private Button saveSettingsButton;
    private Spinner spinner;
    private RadioButton smallFontRadioButton, bigFontRadioButton;
    private Switch showDeleteButtons;
    private Boolean fontSize, showButtons;
    private String fontColor;
    private static SharedPreferences prefs;
    private int number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        saveSettingsButton = (Button) findViewById(R.id.saveSettingsButton);
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        smallFontRadioButton = (RadioButton) findViewById(R.id.smallFontRadioButton);
        bigFontRadioButton = (RadioButton) findViewById(R.id.bigFontRadioButton);
        showDeleteButtons = (Switch) findViewById(R.id.showDeleteButtons);

        spinner.setAdapter(spinnerAdapter);
        spinnerAdapter.add("Czerwony");
        spinnerAdapter.add("Niebieski");
        spinnerAdapter.add("Zielony");
        spinnerAdapter.add("Szary (default)");

        prefs = getSharedPreferences("Settings", 0);

        if (prefs.contains("FontColor")){
            fontColor = prefs.getString("FontColor", "");
            number = spinner.getSelectedItemPosition();
            spinner.setSelection(getIndex(spinner, fontColor));
            fontSize = prefs.getBoolean("FontSize", true);
            smallFontRadioButton.setChecked(fontSize);
            bigFontRadioButton.setChecked(!fontSize);
            showButtons = prefs.getBoolean("ShowDeleteButtons", false);
            showDeleteButtons.setChecked(showButtons);
        } else {
            spinner.setSelection(3);
        }

//        Toast t = Toast.makeText(getApplicationContext(), fontColor + fontSize + showButtons, Toast.LENGTH_SHORT);
//        t.show();

        changeFont();
        saveSettings();
        showButtons();
    }

    public void showButtons(){
        showDeleteButtons.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    showDeleteButtons.setChecked(prefs.getBoolean("FontSize", true));
                }else{
                    showDeleteButtons.setChecked(prefs.getBoolean("FontSize", false));
                }
            }
        });
    }

    public void changeFont(){
        smallFontRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bigFontRadioButton.setChecked(false);
            }
        });

        bigFontRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smallFontRadioButton.setChecked(false);
            }
        });
    }

    public void saveSettings(){
        saveSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSettings(prefs, spinner.getSelectedItem().toString(), smallFontRadioButton.isChecked() , showDeleteButtons.isChecked(), getApplicationContext());
//                Toast t = Toast.makeText(getApplicationContext(), spinner.getSelectedItem().toString() + fontSize + showButtons, Toast.LENGTH_SHORT);
//                t.show();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public static void setSettings(SharedPreferences prefs, String FontColor, Boolean FontSize, boolean ShowDeleteButtons, Context context) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("FontColor", FontColor);
        if (FontSize == true){
            editor.putBoolean("FontSize", true);
        } else {
            editor.putBoolean("FontSize", false);
        }
        editor.putBoolean("ShowDeleteButtons", ShowDeleteButtons);
        editor.commit();
    }

    private int getIndex(Spinner spinner, String s){
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(s)){
                index = i;
            }
        }
        return index;
    }
}
