package com.example.kropp.mini_projekt1.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.*;

public class ProductDBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "product.db";

    public ProductDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 9);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE product_table ( ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, ISSELECTED FALSE)");
    }

    public void getAll(){
        SQLiteDatabase exp = this.getWritableDatabase();
        exp.execSQL("GET * FROM product_table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void EditName(String old_name, String new_name) {
        SQLiteDatabase exp = this.getWritableDatabase();
        exp.execSQL("UPDATE product_table SET NAME = '" + new_name + "' WHERE NAME = '" + old_name +"'");
    }

    public void onDeleteSelected(String name) {
        SQLiteDatabase exp = this.getWritableDatabase();
        exp.execSQL("DELETE FROM product_table WHERE NAME = '" + name + "'");
    }

    public void onDeleteAll() {
        SQLiteDatabase exp = this.getWritableDatabase();
        exp.execSQL("DELETE FROM product_table");
    }

    public void onInsert(String name) {
        SQLiteDatabase exp = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("NAME", name);
        exp.insert("product_table", name, values);
    }

    public boolean insertProduct(String name){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("NAME", name);
        long result = db.insert("product_table", null, values);
        if(result == -1)
            return false;
        else
            return true;
    }

    public Cursor showAllProducts() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM product_table", null);
        return res;
    }


}
