package com.example.kropp.mini_projekt1.Models;

public class Product {

    int ID;
    String name;
    Boolean isSelected;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    @Override
    public String toString() {
        return "Product{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }

}
