package com.example.kropp.mini_projekt1.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kropp.mini_projekt1.Database.ProductDBHelper;
import com.example.kropp.mini_projekt1.R;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private ProductDBHelper db;
    private Button addProduct, showList, settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new ProductDBHelper(this);

        addProduct = (Button) findViewById(R.id.addProduct);
        addProduct();
        showList = (Button) findViewById(R.id.showListButton);
        showProducts();
        settings = (Button) findViewById(R.id.settings);
        changeSettings();

    }

    private View.OnClickListener addNewProduct(){
        return new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddProduct.class);
                startActivity(intent);
            }
        };
    }

    public void addProduct(){
        addProduct.setOnClickListener(addNewProduct());
    }

    private View.OnClickListener showAllProducts(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cursor res = db.showAllProducts();
                if (res.getCount() == 0) {
                    Context context = getApplicationContext();
                    Toast toast = Toast.makeText(context,"Brak produktów do wyświetlenia", Toast.LENGTH_SHORT);
                    toast.show();
                }

                if (res.getCount() != 0) {
                    Intent intent = new Intent(MainActivity.this, ShowProducts.class);
                    startActivity(intent);
                }
            }
        };
    }

    public void showProducts(){
        showList.setOnClickListener(showAllProducts());
    }

    private View.OnClickListener showSettings(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Settings.class);
                startActivity(intent);
            }
        };
    }

    public void changeSettings(){
        settings.setOnClickListener(showSettings());
    }
}
