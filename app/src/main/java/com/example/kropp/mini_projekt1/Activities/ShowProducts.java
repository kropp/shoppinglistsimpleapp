package com.example.kropp.mini_projekt1.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kropp.mini_projekt1.Database.ProductDBHelper;
import com.example.kropp.mini_projekt1.R;

import java.util.ArrayList;

import static com.example.kropp.mini_projekt1.R.id.productList;

public class ShowProducts extends AppCompatActivity {

    private ProductDBHelper db;
    private ArrayList<String> productlist = new ArrayList<String>();
    private ArrayAdapter<String> adapter;
    private ListView productListView;
    private Button deleteSelectedButton, deleteAllButton;
    private SharedPreferences prefs;
    private String fontColor;
    private boolean fontSize, showButtons;
    private TextView infoTextView;
    private EditText editText;
    private Cursor res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_products);

        db = new ProductDBHelper(this);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_multiple_choice, productlist);
        productListView = (ListView) findViewById(productList);
        productListView.setAdapter(adapter);
        productListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        deleteSelectedButton = (Button) findViewById(R.id.deleteSelectedButton);
        deleteAllButton = (Button) findViewById(R.id.deleteAllButton);
        infoTextView = (TextView) findViewById(R.id.infoTexView);
        res = db.showAllProducts();


        productListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ShowProducts.this);
                alertDialog.setTitle("Edytuj " + productListView.getItemAtPosition(position).toString());
                alertDialog.setMessage("Podaj nową nazwę:");

                final EditText input = new EditText(ShowProducts.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);

                alertDialog.setPositiveButton("Zapisz",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                db.EditName(productListView.getItemAtPosition(position).toString(), input.getText().toString());
                                adapter.notifyDataSetChanged();
                                productListView.invalidateViews();
                                dialog.cancel();
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }
                        });

                alertDialog.setNegativeButton("Anuluj",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();
                return false;
            }
        });

        prefs = getSharedPreferences("Settings", 0);

        fontColor = prefs.getString("FontColor", "BLUE");
        fontSize = prefs.getBoolean("FontSize", true);
        showButtons = prefs.getBoolean("ShowDeleteButtons", false);

        if (res.getCount() == 0) {
            Context context = getApplicationContext();
            Toast toast = Toast.makeText(context,"Brak produktów do wyświetlenia", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            StringBuffer buffer = new StringBuffer();

            while (res.moveToNext()) {
                productlist.add(res.getString(1).toString());
            }
        }

        getSettings();
        deleteAll();
        deleteSelected();

    }

    private void getSettings() {
        if (fontColor == "Czerwony"){
            infoTextView.setTextColor(Color.RED);
        } else if (fontColor == "Niebieski"){
            infoTextView.setTextColor(Color.BLUE);
        } else if (fontColor == "Zielony"){
            infoTextView.setTextColor(Color.GREEN);
        } else {
            infoTextView.setTextColor(Color.GRAY);
        }

        if (fontSize == true){
            infoTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, 50);
        } else {
            infoTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, 80);
        }

        if (showButtons == true){
                deleteAllButton.setVisibility(View.VISIBLE);
                deleteSelectedButton.setVisibility(View.VISIBLE);
            } else {
                deleteAllButton.setVisibility(View.GONE);
                deleteSelectedButton.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener deleteAllOnClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cursor res = db.showAllProducts();
                Context context = getApplicationContext();

                if (res.getCount() == 0) {
                    Toast toast = Toast.makeText(context,"Brak produktów do usunięcia", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    db.onDeleteAll();
                    Toast toast = Toast.makeText(context,"Usunięto wszystkie produkty", Toast.LENGTH_SHORT);
                    Intent intent = new Intent(ShowProducts.this, MainActivity.class);
                    startActivity(intent);
                    toast.show();

                }
            }
        };
    }

    public void deleteAll(){
        deleteAllButton.setOnClickListener(deleteAllOnClickListener());
    }

    private View.OnClickListener deleteSelectedOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selected = "";
                int cntChoice = productListView.getCount();
                SparseBooleanArray sparseBooleanArray = productListView.getCheckedItemPositions();

                for (int i = 0; i < cntChoice; i++) {
                    if (sparseBooleanArray.get(i)) {
                        selected += productListView.getItemAtPosition(i).toString() + "\n";
                    }
                }

                String[] selectedItems = selected.split("\n");

                for(String item : selectedItems){
                    adapter.remove(item);
                    db.onDeleteSelected(item);
                }

                productListView.clearChoices();
                adapter.notifyDataSetChanged();
                Toast.makeText(ShowProducts.this, "Usunięto zaznaczone produkty", Toast.LENGTH_LONG).show();
            }
        };
    }

    public void deleteSelected(){
        deleteSelectedButton.setOnClickListener(deleteSelectedOnClickListener());
    }
}